package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("El ID del producto a buscar: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("Id del producto: " + product.getId());
        System.out.println("Descripcion del producto: " + product.getDesc());
        System.out.println("Precio del producto: " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("Id del producto a actualizar: " + id);
        System.out.println("Id de producto nuevo: " + product.getId());
        System.out.println("Descripcion del producto nuevo: " + product.getDesc());
        System.out.println("Precio del producto nuevo: " + product.getPrice());

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? this.productService.updateById(id, product) : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("Id del producto a eliminar: " + id);

        boolean found = this.productService.findById(id).isPresent();
        if (found) this.productService.deleteById(id);

        return new ResponseEntity<>(
                found ? "Producto borrado": "Producto no encontrado",
                found ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
