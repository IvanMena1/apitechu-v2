package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(required = false) String orderBy) {
        System.out.println("En getUsers()");
        System.out.println("Query param sort: " + orderBy);

        int sort = (orderBy != null) ? (orderBy.equals("age") ? 1 : (orderBy.equals("name") ? 2 : 0)) : 0;

        return new ResponseEntity<>(

                sort == 1 ? this.userService.findAllSortByAge() : ( sort == 2 ? this.userService.findAllSortByName() : this.userService.findAll()),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("En getUserById()");
        System.out.println("id de usuario a buscar: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("En addUser()");
        System.out.println("Id nuevo: " + user.getId());
        System.out.println("Nombre nuevo: " + user.getName());
        System.out.println("Edad nueva: " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.OK
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUserById(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("En updateUserById()");
        System.out.println("Usuario a actualizar: " + id);
        System.out.println("Datos nuevos: " + user.getName() + ", " + user.getAge());

        if (user.getName() == null || user.getAge() == 0) return new ResponseEntity<>("Campos obligatorios no informados", HttpStatus.BAD_REQUEST);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? this.userService.updateById(id, user) : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUserById(@PathVariable String id) {
        System.out.println("En deleteUserById()");
        System.out.println("Usuario a eliminar: " + id);

        boolean found = this.userService.findById(id).isPresent();
        if (found) this.userService.deleteById(id);

        return new ResponseEntity<>(
                found ? "Usuario eliminado" : "Usuario no encontrado",
                found ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
