package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel> findAll() {
        System.out.println("findAll() UserService");

        return this.userRepository.findAll();
    }

    public List<UserModel> findAllSortByAge() {
        System.out.println("findAllSortByAge() UserService");

        return this.userRepository.findAll(Sort.by(Sort.DEFAULT_DIRECTION,"age"));
    }

    public List<UserModel> findAllSortByName() {
        System.out.println("findAllSortByName() UserService");

        return this.userRepository.findAll(Sort.by(Sort.DEFAULT_DIRECTION,"name"));
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById() UserService");

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user) {
        System.out.println("add() UserService");

        return this.userRepository.save(user);
    }

    public Object updateById(String id, UserModel user) {
        System.out.println("updateById() UserService");

        user.setId(id);
        return this.userRepository.save(user);
    }

    public void deleteById(String id) {
        System.out.println("deleteById() UserService");

        this.userRepository.deleteById(id);
    }
}
