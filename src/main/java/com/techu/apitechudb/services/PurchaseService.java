package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll() {
        System.out.println("findAll() PurchaseService");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById() PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    public PurchaseServiceResponse add(PurchaseModel purchase) {
        System.out.println("add() PurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        if (this.userService.findById(purchase.getUserId()).isEmpty()) {
            result.setMsg("Usuario no encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.findById(purchase.getId()).isPresent()) {
            result.setMsg("La compra con ese ID ya existe");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float totalAmount = 0;
        Optional<ProductModel> product;
        for (Map.Entry<String, Integer> item : purchase.getPurchaseItems().entrySet()) {
            product = this.productService.findById(item.getKey());
            if (product.isEmpty()) {
                result.setMsg("Producto " + item.getKey() + " no encontrado");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            }
            totalAmount += (product.get().getPrice() * item.getValue());
        }

        System.out.println("Monto total compra: " + totalAmount);
        purchase.setAmount(totalAmount);

        this.purchaseRepository.save(purchase);

        result.setPurchase(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }
}
