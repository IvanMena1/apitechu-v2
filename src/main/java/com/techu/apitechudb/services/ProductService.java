package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll() ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById() ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product) {
        System.out.println("add() ProductService");

        return this.productRepository.save(product);
    }

    public ProductModel updateById(String id, ProductModel product) {
        System.out.println("updateById() ProductService");

        this.deleteById(id);
        return this.productRepository.save(product);
    }

    public void deleteById(String id) {
        System.out.println("deleteById() ProductService");

        this.productRepository.deleteById(id);
    }
}
